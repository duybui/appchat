//
//  AppDelegate.swift
//  AppChat
//
//  Created by Duy Bui on 2/6/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit
import Firebase
import HockeySDK
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  var authListener: AuthStateDidChangeListenerHandle?
  
  var locationManager: CLLocationManager?
  var coordinates: CLLocationCoordinate2D?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    BITHockeyManager.shared().configure(withIdentifier: "b02b6dc1ccde4804ac0f98307c346183")
    BITHockeyManager.shared().crashManager.crashManagerStatus = BITCrashManagerStatus.autoSend
    BITHockeyManager.shared().start()
    BITHockeyManager.shared().authenticator.authenticateInstallation()
    
    FirebaseApp.configure()
    let db = Firestore.firestore()
    let settings = db.settings
    settings.areTimestampsInSnapshotsEnabled = true
    db.settings = settings
    
    authListener = Auth.auth().addStateDidChangeListener({ (auth, user) in
      Auth.auth().removeStateDidChangeListener(self.authListener!)
      if user != nil {
        if UserDefaults.standard.object(forKey: kCURRENTUSER) != nil {
          DispatchQueue.main.async {
            Application.shared.homeScreen()
          }
        }
      }
    })
    Application.shared.startWalkthroughScreen()
    return true
  }
  
  func applicationDidEnterBackground(_ application: UIApplication) {
    locationManagerStop()
  }
  
  func applicationDidBecomeActive(_ application: UIApplication) {
    locationManagerStart()
  }
}

extension AppDelegate: CLLocationManagerDelegate {
  
  //MARK: - Location Manager
  func locationManagerStart() {
    if locationManager == nil {
      locationManager = CLLocationManager()
      locationManager?.delegate = self
      locationManager?.desiredAccuracy = kCLLocationAccuracyBest
      locationManager?.requestWhenInUseAuthorization()
    }
    
    locationManager?.startUpdatingLocation()
  }
  
  func locationManagerStop() {
    if locationManager != nil {
      locationManager?.stopUpdatingLocation()
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    print("faild to get Location")
  }
  
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    switch status {
    case .notDetermined:
      manager.requestWhenInUseAuthorization()
    case .authorizedWhenInUse:
      manager.startUpdatingLocation()
    case .authorizedAlways:
      manager.startUpdatingLocation()
    case .restricted:
      print("restricted")
    case .denied:
      locationManager = nil
      print("Denied Location")
      break
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    coordinates = locations.last!.coordinate
  }
}
