//
//  Application.swift
//  AppChat
//
//  Created by Duy Bui on 2/6/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit

class Application {
  
  var navigator = UIStoryboard.Screen.navigator
  static var shared = Application()
  
  private init() {}
  
  func startWalkthroughScreen() {
    guard let firstScreen = UIStoryboard.Screen.walkthrough else { return }
    self.navigator?.viewControllers = [firstScreen]
    ApplicationDelegate.appDelegate?.window?.rootViewController = self.navigator
  }
  
  func homeScreen() {
    
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: USER_DID_LOGIN_NOTIFICATION), object: nil, userInfo: [kUSERID: FUser.currentId()])
    
    guard let tabBar = UIStoryboard.Screen.tabBar else { return }
    self.navigator?.viewControllers = [tabBar]
    ApplicationDelegate.appDelegate?.window?.rootViewController = self.navigator
  }
}

struct ApplicationDelegate {
  
  static var appDelegate: AppDelegate? {
    return UIApplication.shared.delegate as? AppDelegate
  }
}
