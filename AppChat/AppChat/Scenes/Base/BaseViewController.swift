//
//  BaseViewController.swift
//  AppChat
//
//  Created by Duy Bui on 2/6/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit
import RxSwift
import UserNotifications
import NVActivityIndicatorView

protocol LoadingProtocol {
  
  func showActivityIndicatorUseCase()
  func hideActivityIndicatorUseCase()
}

class BaseViewController: UIViewController, NVActivityIndicatorViewable {
  
  private let disposeBag = DisposeBag()
  var reachability: Reachability?
  var isGrantedUserNotificationsAccess = false
  var isReConnectInternet = false
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    
    reachability = Reachability()
    
    bindReachability()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    try? reachability?.startNotifier()
    
    if (self is WalkthroughViewController ||
      self is HomeViewController) {
      navigationController?.navigationBar.isHidden = true
    } else {
      navigationController?.navigationBar.isHidden = false
      // Custom NAV background color
      customNavigationBarBackgroundColor()
    }
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    
    reachability?.stopNotifier()
  }
  
  // Hide keyboard when user touches outside keyboard
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    self.view.endEditing(true)
  }
  
  // MARK: - Activity Indicator
  func showActivityIndicator() {
    
    // Set font size of message displayed in UI blocked
    NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE_FONT =  UIFont.systemFont(ofSize: 14)
    
    // Show activity indicator
    DispatchQueue.main.async { [weak self] in
      self?.startAnimating(Constant.Size.activityIndicator, type: .lineSpinFadeLoader, color: UIColor.InApp.blue)
    }
  }
  
  func hideActivityIndicator() {
    
    // Stop indicator
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Constant.Number.hideIndicatorDelay) { [weak self] in
      self?.stopAnimating()
    }
  }
  
  // MARK: - Navigation
  /// Setup navigation background color
  ///
  /// - Parameters:
  ///   - color: The UI color
  ///   - isTransparent: In the case exist Transparent
  func setupNavigationWithBackgroundColor(color: UIColor, isTransparent: Bool = true) {
    navigationController?.navigationBar.shadowImage = UIImage()
    navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationController?.navigationBar.isTranslucent = isTransparent
    navigationController?.navigationBar.backgroundColor = color
  }
  
  /// Setup title navigation
  ///
  /// - Parameters:
  ///   - title: The title navigation
  ///   - fontColor: The UI color
  ///   - font: The custom font
  private func setupTitleNavigation(title: String, fontColor: UIColor, font: UIFont? = UIFont.systemFont(ofSize: 14)) {
    let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width/1.5, height: 40))
    label.backgroundColor = .clear
    label.numberOfLines = 0
    label.textAlignment = .center
    label.attributedText = NSAttributedString(string: title, attributes: [NSAttributedString.Key.foregroundColor: fontColor, NSAttributedString.Key.font: font ?? [], NSAttributedString.Key.kern: 1])
    
    label.sizeToFit()
    label.minimumScaleFactor = 0.25
    label.adjustsFontSizeToFitWidth = true
    self.navigationItem.titleView = label
  }
  
  /// Setup left navigation button with icon
  ///
  /// - Parameters:
  ///   - name: The button's name icon
  ///   - isTransparent: In the case exist Transparent
  private func setupLeftNavigationButtonWithIcon(name: String, popToVC: Bool = false, isMenu: Bool = false) {
    let leftButtonImage: UIImage? = UIImage(named: name)?.withRenderingMode(.alwaysOriginal)
    let leftButton = UIButton(type: UIButton.ButtonType.custom)
    leftButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
    leftButton.imageEdgeInsets = .zero
    leftButton.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
    leftButton.setImage(leftButtonImage, for: UIControl.State.normal)
    
    if isMenu {
      leftButton.addTarget(self, action: #selector(backToVC(sender:)), for: .touchDown)
    } else if popToVC {
      leftButton.addTarget(self, action: #selector(backToVC(sender:)), for: .touchDown)
    }
    
    let leftBarButton = UIBarButtonItem()
    leftBarButton.customView = leftButton
    self.navigationItem.leftBarButtonItem = leftBarButton
  }
  
  /// Back to setting view controller
  ///
  /// - Parameters:
  ///   - sender: The sender object
  @objc func backToVC(sender: AnyObject) {
    guard let nav = self.navigationController else { return }
    if self is LoginViewController || self is SignupViewController {
      nav.popToRootViewController(animated: true)
    } else {
      nav.popViewController(animated: true)
    }
  }
  
  /// Custom UI Navigation bar
  ///
  /// - Parameters:
  ///   - title: The title bar
  ///   - icon: The Icon back button
  ///   - fontColor: The font color
  ///   - font: The font family
  func setupUINavigationBar (title: NavBarTitle = .splash,
                             icon: NavBarIcon = .back,
                             fontColor: UIColor =  UIColor.InApp.blue,
                             font: UIFont? = UIFont.systemFont(ofSize: 14),
                             subTitle: String = "") {
    
    navigationController?.isNavigationBarHidden = false
    
    switch title {
      
    case .splash:
      
      setupLeftNavigationButtonWithIcon(name: icon.name, popToVC: true, isMenu: false)
      
    default:
      setupTitleNavigation(title: title.name, fontColor: fontColor, font: font)
      setupLeftNavigationButtonWithIcon(name: icon.name, popToVC: true, isMenu: false)
    }
  }
  
  /// Setup Right Bar Button Item
  ///
  /// - Parameters:
  ///   - name: The name of button
  func setupRightNavigationButton(name: String) {
    let rightButton = UIButton(type: .system)
    rightButton.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
    rightButton.setTitle(name, for: .normal)
    rightButton.contentHorizontalAlignment = .right
    rightButton.setTitleColor(UIColor.InApp.blue, for: .normal)
    rightButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
    rightButton.addTarget(self, action: #selector(didTapOnRightButton(sender:)), for: .touchUpInside)
    let rightBarButtonItem = UIBarButtonItem()
    rightButton.sizeToFit()
    rightBarButtonItem.customView = rightButton
    self.navigationItem.rightBarButtonItem = rightBarButtonItem
  }
  
  @objc func didTapOnRightButton(sender: AnyObject) {
    // Determine to override in specical viewcontroller
  }
  
  private func customNavigationBarBackgroundColor() {
    switch self {
      //    case is LoginViewController,
      //         is WalkthroughViewController:
      //      self.setupNavigationWithBackgroundColor(color: UIColor.white)
      
    default:
      self.setupNavigationWithBackgroundColor(color: UIColor.clear)
    }
  }
}

// Subscribe reachability
extension BaseViewController {
  
  func bindReachability() {
    
    reachability?.rx.isDisconnected.subscribe(onNext: {[weak self] in
      guard let `self` = self else { return }
      self.isReConnectInternet = true
      showNotificationBanner(messageContent: "No network connection. Please check your internet and try again.",
                             bannerBackgroundColor: UIColor.InApp.blue,
                             messageImageName: "icon_off_internet_connection")
    }).disposed(by: disposeBag)
    
    reachability?.rx.isConnected.subscribe(onNext: {[weak self] in
      guard let `self` = self else { return }
      dissmissBanner(completion: { [weak self] (_) in
        guard let `self` = self else { return }
        if self.isReConnectInternet {
          showNotificationBanner(messageContent: "You are connected now.",
                                 bannerBackgroundColor: UIColor.InApp.blue,
                                 isAutoDismiss: true, messageImageName: "icon_on_internet_connection")
        }
      })
    }).disposed(by: disposeBag)
  }
}

extension BaseViewController: LoadingProtocol {
  
  func showActivityIndicatorUseCase() {
    showActivityIndicator()
  }
  
  func hideActivityIndicatorUseCase() {
    hideActivityIndicator()
  }
  
  func dismissKeyboard() {
    self.view.endEditing(false)
  }
}

