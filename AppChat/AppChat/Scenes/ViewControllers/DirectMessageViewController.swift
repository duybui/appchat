//
//  DirectMessageViewController.swift
//  WhosWin
//
//  Created by Duy Bui on 3/10/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import ProgressHUD
import IQAudioRecorderController
import IDMPhotoBrowser
import AVFoundation
import AVKit
import FirebaseFirestore

class DirectMessageViewController: JSQMessagesViewController {
  
  // delegate
  let appDelegate = UIApplication.shared.delegate as! AppDelegate
  
  // listeners
  var typingListener: ListenerRegistration?
  var updateListener: ListenerRegistration?
  var newChatListener: ListenerRegistration?
  
  // variable
  var chatRoomId: String?
  var memberIds: [String]?
  var membersToPush: [String]?
  var titleName: String?
  var withUsers: [FUser] = []
  var isGroup: Bool?
  var group: NSDictionary?
  
  // types
  let legitTypes = [kAUDIO, kVIDEO, kTEXT, kLOCATION, kPICTURE]
  
  // message
  var messages: [JSQMessage] = []
  var objectMessages: [NSDictionary] = []
  var loadedMessages: [NSDictionary] = []
  var allPictureMessages: [String] = []
  var initialLoadComplete = false
  
  // count
  var maxMessageNumber = 0
  var minMessageNumber = 0
  var loadOld = false
  var loadedMessagesCount = 0
  
  var outgoingBubble = JSQMessagesBubbleImageFactory()?.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
  var incomingBubble = JSQMessagesBubbleImageFactory()?.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
  
  //MARK: Custom Headers
  let leftBarButtonView: UIView = {
    let view = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 44))
    return view
  }()
  
  let avatarButton: UIButton = {
    let button = UIButton(frame: CGRect(x: 0, y: 10, width: 25, height: 25))
    return button
  }()
  
  let titleLabel: UILabel = {
    let title = UILabel(frame: CGRect(x: 30, y: 10, width: 140, height: 15))
    title.textAlignment = .left
    title.font = UIFont(name: title.font.fontName, size: 14)
    return title
  }()
  
  let subTitle: UILabel = {
    let title = UILabel(frame: CGRect(x: 30, y: 25, width: 140, height: 15))
    title.textAlignment = .left
    title.font = UIFont(name: title.font.fontName, size: 10)
    return title
  }()
  
  // fix for Iphone x
  override func viewDidLayoutSubviews() {
    perform(Selector(("jsq_updateCollectionViewInsets")))
  }
  // end fix for Iphone x
  override func viewDidLoad() {
    super.viewDidLoad()
    /// custom back button
    // un-comment here if needed
    // video 4 - part
//    self.navigationItem.leftBarButtonItems = [UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(self.backAction))]
    
    collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
    collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
    
    setCustomTitle()
    loadMessage()
    self.senderId = FUser.currentId()
    self.senderDisplayName = FUser.currentUser()?.firstname
    
    // fix for IPhone X
    if let constraints = perform(Selector(("toolbarBottomLayoutGuide")))?.takeUnretainedValue() as? NSLayoutConstraint {
      constraints.priority = UILayoutPriority(rawValue: 999)
      if #available(iOS 11.0, *) {
        self.inputToolbar.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
      } else {
        // Fallback on earlier versions
        print("Here")
      }
    }
    
    // custom mic button
    self.inputToolbar.contentView.rightBarButtonItem.setImage(UIImage(named: "mic"), for: .normal)
    self.inputToolbar.contentView.rightBarButtonItem.setTitle("", for: .normal)
  }
  
  private func setCustomTitle() {
    leftBarButtonView.addSubview(avatarButton)
    leftBarButtonView.addSubview(titleLabel)
    leftBarButtonView.addSubview(subTitle)
    
    let infoButton = UIBarButtonItem(image: UIImage(named: "info"), style: .plain, target: self, action: #selector(self.infoButtonPressed))
    
    self.navigationItem.rightBarButtonItem = infoButton
    
    let leftBarButtonItem = UIBarButtonItem(customView: leftBarButtonView)
    self.navigationItem.leftBarButtonItems?.append(leftBarButtonItem)
    
    if isGroup! {
      avatarButton.addTarget(self, action: #selector(self.showGroup), for: .touchUpInside)
    }else {
      avatarButton.addTarget(self, action: #selector(self.showUserProfile), for: .touchUpInside)
    }
    
    getUsersFromFirestore(withIds: memberIds ?? []) { (withUsers) in
      self.withUsers = withUsers
      
      if !self.isGroup! {
        self.setUIForSingleChat()
      }
    }
  }
  
  @objc func infoButtonPressed() {
    print("show image message")
  }
  
  @objc func showGroup() {
    print("show Group")
  }
  
  @objc func showUserProfile() {
    // withUser.first to get data
  }
  
  func setUIForSingleChat() {
    let withUser = withUsers.first!
    imageFromData(pictureData: withUser.avatar) { (image) in
      if image != nil {
        avatarButton.setImage(image!.circleMasked, for: .normal)
      }
    }
    
    titleLabel.text = withUser.fullname
    if withUser.isOnline {
      subTitle.text = "Online"
    }else {
      subTitle.text = "Offline"
    }
    
    avatarButton.addTarget(self, action: #selector(showUserProfile), for: .touchUpInside)
  }
  
  // MARK: - JSQMessages DataSource functions
  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as? JSQMessagesCollectionViewCell else {
      return UICollectionViewCell()
    }
    let data = messages[indexPath.row]
    
    // set text color
    if data.senderId == FUser.currentId() {
      cell.textView?.textColor = .white
    } else {
      cell.textView?.textColor = .black
    }
    return cell
  }
  
  override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
    return messages[indexPath.row]
  }
  
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return messages.count
  }
  
  override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
    let data = messages[indexPath.row]
    if data.senderId == FUser.currentId() {
      return outgoingBubble
    } else {
      return incomingBubble
    }
  }
  
  override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
    if indexPath.item % 3 == 0 {
      return kJSQMessagesCollectionViewCellLabelHeightDefault
    } else {
      return 0.0
    }
  }
  
  override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAt indexPath: IndexPath!) -> NSAttributedString! {
    let message = objectMessages[indexPath.row]
    let status: NSAttributedString?
    let attributedStringColor = [NSAttributedString.Key.foregroundColor: UIColor.darkGray]
    guard let messageStatus = message[kSTATUS] as? String else { return NSAttributedString(string: "✕")}
    switch messageStatus {
    case kDELIVERED:
      status = NSAttributedString(string: kDELIVERED)
    case kREAD:
      let statusText = "Read" + " " + readTimeFrom(dateString: (message[kREADDATE] as? String) ?? "")
      status = NSAttributedString(string: statusText, attributes: attributedStringColor)
    default:
      status = NSAttributedString(string: "✔︎")
    }
    
    if indexPath.row == (messages.count - 1) {
      return status
    } else {
      return NSAttributedString(string: "")
    }
  }
  
  override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
    let data = messages[indexPath.row]
    if data.senderId == FUser.currentId() {
      return kJSQMessagesCollectionViewCellLabelHeightDefault
    } else {
      return 0.0
    }
  }
  
  // MARK: - JSQMessages Delegate functions
  override func didPressAccessoryButton(_ sender: UIButton!) {
    let camera = Camera(delegate_: self)
    let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    let takePhotoOrVideo = UIAlertAction(title: "Camera", style: .default) { (action) in
      camera.PresentMultyCamera(target: self, canEdit: false)
    }
    let sharePhoto = UIAlertAction(title: "Photo Library", style: .default) { (action) in
      camera.PresentPhotoLibrary(target: self, canEdit: false)
    }
    let shareVideo = UIAlertAction(title: "Video Library", style: .default) { (action) in
      camera.PresentVideoLibrary(target: self, canEdit: false)
    }
    let shareLocation = UIAlertAction(title: "Share Location", style: .default) { (action) in
      if self.haveAccessToUserLocation() {
        self.sendMessage(text: nil, date: Date(), picture: nil, location: kLOCATION, video: nil, audio: nil)
      }
    }
    let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action) in
      print("Cancel")
    }
    takePhotoOrVideo.setValue(UIImage(named: "camera"), forKey: "image")
    sharePhoto.setValue(UIImage(named: "picture"), forKey: "image")
    shareVideo.setValue(UIImage(named: "video"), forKey: "image")
    shareLocation.setValue(UIImage(named: "location"), forKey: "image")
    
    optionMenu.addAction(takePhotoOrVideo)
    optionMenu.addAction(sharePhoto)
    optionMenu.addAction(shareVideo)
    optionMenu.addAction(shareLocation)
    optionMenu.addAction(cancelAction)
    
    self.present(optionMenu, animated: true, completion: nil)
  }
  
  override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
    if !text.isEmpty {
      self.sendMessage(text: text, date: date, picture: nil, location: nil, video: nil, audio: nil)
      updateSendButton(isSend: false)
    } else {
      // Audio message
      let audioVC = AudioViewController(delegate_: self)
      audioVC.presentAudioRecorder(target: self)
    }
  }
  
  override func collectionView(_ collectionView: JSQMessagesCollectionView!, header headerView: JSQMessagesLoadEarlierHeaderView!, didTapLoadEarlierMessagesButton sender: UIButton!) {
    loadMoreMessages(maxNumber: maxMessageNumber, minNumber: minMessageNumber)
    collectionView.reloadData()
  }
  
  override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
    print("tap on message at \(indexPath!)")
    
    let messageDictionary = objectMessages[indexPath.row]
    let messageType = messageDictionary[kTYPE] as! String
    
    switch messageType {
    case kPICTURE:
      let message = messages[indexPath.row]
      let mediaItem = message.media as! PhotoMediaItem
      let photos = IDMPhoto.photos(withImages: [mediaItem.image])
      let browser = IDMPhotoBrowser(photos: photos)
      self.present(browser!, animated: true, completion: nil)
    case kLOCATION:
      let message = messages[indexPath.row]
      let mediaItem = message.media as! JSQLocationMediaItem
      guard let mapView = UIStoryboard.Screen.map else { return }
      mapView.location = mediaItem.location
      self.navigationController?.pushViewController(mapView, animated: true)
    case kVIDEO:
      let message = messages[indexPath.row]
      let mediaItem = message.media as! VideoMessage
      let player = AVPlayer(url: mediaItem.fileURL! as URL)
      let moviePlayer = AVPlayerViewController()
      let session = AVAudioSession.sharedInstance()
      try! session.setCategory(.playAndRecord, mode: .default, options: .defaultToSpeaker)
      moviePlayer.player = player
      self.present(moviePlayer, animated: true) {
        moviePlayer.player!.play()
      }
    default:
      print("unknow media type")
    }
  }
  
  func loadMoreMessages(maxNumber: Int, minNumber: Int) {
    if loadOld {
      maxMessageNumber = minNumber - 1
      minMessageNumber = maxMessageNumber - kNUMBEROFMESSAGES
    }
    
    if minMessageNumber < 0 {
      minMessageNumber = 0
    }
    
    for i in (minMessageNumber ... maxMessageNumber).reversed() {
      let messageDictionary = loadedMessages[i]
      insertNewMessage(messageDictionary: messageDictionary)
      loadedMessagesCount += 1
    }
    
    loadOld = true
    showLoadEarlierMessagesHeader = (loadedMessagesCount != loadedMessages.count)
  }
  
  func insertNewMessage(messageDictionary: NSDictionary) {
    let incomingMessage = IncomingMessage(collectionView: collectionView)
    let message = incomingMessage.createMessage(messageDictionary: messageDictionary, chatroomId: chatRoomId ?? "")
    objectMessages.insert(messageDictionary, at: 0)
    messages.insert(message!, at: 0)
  }
  
  // MARK: - Update Send Button
  func updateSendButton(isSend: Bool) {
    if isSend {
      self.inputToolbar.contentView.rightBarButtonItem.setImage(UIImage(named: "send"), for: .normal)
    } else {
      self.inputToolbar.contentView.rightBarButtonItem.setImage(UIImage(named: "mic"), for: .normal)
    }
  }
  
  // MARK: - Update Text Field
  override func textViewDidChange(_ textView: UITextView) {
    if !textView.text.isEmpty {
      updateSendButton(isSend: true)
    } else {
      updateSendButton(isSend: false)
    }
  }
  
  // MARK: - Send Messages
  func sendMessage(text: String?, date: Date, picture: UIImage?, location: String?, video: NSURL?, audio: String?) {
    var outgoingMessage: OutgoingMessage?
    let currentUser = FUser.currentUser()
    
    // text message
    if let text = text, let currentUser = currentUser {
      outgoingMessage = OutgoingMessage(message: text, senderId: currentUser.objectId, senderName: currentUser.firstname, date: date, status: kDELIVERED, type: kTEXT)
    }
    
    // picture message
    if let pic = picture {
      uploadImage(image: pic, chatRoomId: chatRoomId ?? "", view: self.navigationController!.view) { (imageLink) in
        if imageLink != nil {
          let text = "[\(kPICTURE)]"
          outgoingMessage = OutgoingMessage(message: text, pictureLink: imageLink!, senderId: currentUser?.objectId ?? "", senderName: currentUser?.fullname ?? "", date: date, status: kDELIVERED, type: kPICTURE)
          
          JSQSystemSoundPlayer.jsq_playMessageSentSound()
          self.finishSendingMessage()
          outgoingMessage?.sendMessage(chatRoomId: self.chatRoomId ?? "", messageDictionary: outgoingMessage!.messageDictionary, memberIds: self.memberIds ?? [], memberToPush: self.membersToPush ?? [])
        }
      }
      return
    }
    
    // send video
    if let video = video {
      let videoData = NSData(contentsOfFile: video.path!)
      let thumbNail = videoThumbnail(video: video)
      let dataThumbnail = thumbNail.jpegData(compressionQuality: 0.7)! as NSData
      
      uploadVideo(video: videoData!, chatRoomId: chatRoomId ?? "", view: self.navigationController!.view) { (videoLink) in
        if videoLink != nil {
          let text = "[\(kVIDEO)]"
          outgoingMessage = OutgoingMessage(message: text, video: videoLink ?? "", thumbNail: dataThumbnail, senderId: currentUser?.objectId ?? "", senderName: currentUser?.fullname ?? "", date: date, status: kDELIVERED, type: kVIDEO)
          
          JSQSystemSoundPlayer.jsq_playMessageSentSound()
          self.finishSendingMessage()
          outgoingMessage?.sendMessage(chatRoomId: self.chatRoomId ?? "", messageDictionary: outgoingMessage!.messageDictionary, memberIds: self.memberIds ?? [], memberToPush: self.membersToPush ?? [])
        }
      }
      return
    }
    
    // send audio
    if let audioPath = audio {
      uploadAudio(audioPath: audioPath, chatRoomId: chatRoomId ?? "", view: self.navigationController!.view) { (audioLink) in
        if audioLink != nil {
          let text = "[\(kAUDIO)]"
          let encryptedText = Encryption.encrytText(chatRoomId: self.chatRoomId ?? "", message: text)
          outgoingMessage = OutgoingMessage(message: encryptedText, audio: audioLink!, senderId: currentUser?.objectId ?? "", senderName: currentUser?.fullname ?? "", date: date, status: kDELIVERED, type: kAUDIO)
          
          JSQSystemSoundPlayer.jsq_playMessageSentSound()
          self.finishSendingMessage()
          outgoingMessage?.sendMessage(chatRoomId: self.chatRoomId ?? "", messageDictionary: outgoingMessage!.messageDictionary, memberIds: self.memberIds ?? [], memberToPush: self.membersToPush ?? [])
        }
      }
      return
    }
    
    // send location message
    if location != nil {
      let lat: NSNumber = NSNumber(value: appDelegate.coordinates!.latitude)
      let long: NSNumber = NSNumber(value: appDelegate.coordinates!.longitude)
      
      let text = "[\(kLOCATION)]"
      let encryptedText = Encryption.encrytText(chatRoomId: self.chatRoomId ?? "", message: text)
      
      outgoingMessage = OutgoingMessage(message: encryptedText, latitude: lat, longitude: long, senderId: currentUser?.objectId ?? "", senderName: currentUser?.fullname ?? "", date: date, status: kDELIVERED, type: kLOCATION)
    }
    
    JSQSystemSoundPlayer.jsq_playMessageSentSound()
    self.finishSendingMessage()
    
    if let outgoingMessage = outgoingMessage, let chatRoomId = chatRoomId {
      outgoingMessage.sendMessage(chatRoomId: chatRoomId, messageDictionary: outgoingMessage.messageDictionary, memberIds: memberIds ?? [], memberToPush: membersToPush ?? [])
    }
  }
  
  // MARK: - Load messages
  func loadMessage() {
    // to update message status
    updateListener = reference(.Message).document(FUser.currentId()).collection(chatRoomId ?? "").addSnapshotListener({ (snapshot, error) in
      guard let snapshot = snapshot else { return }
      
      if !snapshot.isEmpty {
        snapshot.documentChanges.forEach({ (diff) in
          if diff.type == .modified {
            // update local message
            self.updateMessage(messageDictionary: diff.document.data() as NSDictionary)
          }
        })
      }
    })
    
    // get last 11 messages
    reference(.Message).document(FUser.currentId()).collection(chatRoomId ?? "").order(by: kDATE, descending: true).limit(to: 11).getDocuments { (snapshot, error) in
      guard let snapshot = snapshot else {
        self.initialLoadComplete = true
        // listen for new chats
        return
      }
      
      let sorted = ((dictionaryFromSnapshots(snapshots: snapshot.documents)) as NSArray).sortedArray(using: [NSSortDescriptor(key: kDATE, ascending: true)]) as? [NSDictionary]
      
      // remove bad message
      self.loadedMessages = self.removeBadMessages(allMessages: sorted ?? [])
      
      //insert message
      self.insertMessages()
      self.finishSendingMessage(animated: true)
      self.initialLoadComplete = true
      
      print("we have \(self.messages.count) messages load")
      // get picture messages
      self.getPictureMessages()
      
      // get old messages in background
      self.getOldMessagesInBackground()
      // start listening for new chats
      self.listenForNewChats()
    }
  }
  
  private func updateMessage(messageDictionary: NSDictionary) {
    for index in 0 ..< objectMessages.count {
      let temp = objectMessages[index]
      if messageDictionary[kMESSAGEID] as! String == temp[kMESSAGEID] as! String {
        objectMessages[index] = messageDictionary
        self.collectionView.reloadData()
      }
    }
  }
  
  func addNewPictureMessageLink(link: String) {
    allPictureMessages.append(link)
  }
  
  func getPictureMessages() {
    allPictureMessages = []
    
    for message in loadedMessages {
      if message[kTYPE] as! String == kPICTURE {
        allPictureMessages.append(message[kPICTURE] as! String)
      }
    }
  }
  
  func getOldMessagesInBackground() {
    if loadedMessages.count > 10 {
      if let firstMessageDate = loadedMessages.first?[kDATE] as? String {
        reference(.Message).document(FUser.currentId()).collection(chatRoomId ?? "").whereField(kDATE, isLessThan: firstMessageDate).getDocuments { (snapshot, error) in
          guard let snapshot = snapshot else { return }
          if let sorted = (dictionaryFromSnapshots(snapshots: snapshot.documents) as NSArray).sortedArray(using: [NSSortDescriptor(key: kDATE, ascending: true)]) as? [NSDictionary] {
            self.loadedMessages = self.removeBadMessages(allMessages: sorted) + self.loadedMessages
            // get the picture messages
            self.maxMessageNumber = self.loadedMessages.count - self.loadedMessagesCount - 1
            self.minMessageNumber = self.maxMessageNumber - kNUMBEROFMESSAGES
          }
        }
      }
    }
  }
  
  func listenForNewChats() {
    let lastMessageDate: String = {
      if loadedMessages.count > 0 {
        return loadedMessages.last?[kDATE] as? String ?? "0"
      } else {
        return "0"
      }
    }()
    newChatListener = reference(.Message).document(FUser.currentId()).collection(chatRoomId ?? "").whereField(kDATE, isGreaterThan: lastMessageDate).addSnapshotListener({ (snapshot, error) in
      guard let snapshot = snapshot else { return }
      if !snapshot.isEmpty {
        for diff in snapshot.documentChanges {
          if (diff.type == .added) {
            let item = diff.document.data() as NSDictionary
            if let type = item[kTYPE] as? String {
              if self.legitTypes.contains(type) {
                // this is for picture messages
                if type == kPICTURE {
                  // add to pictures
                  self.addNewPictureMessageLink(link: item[kPICTURE] as! String)
                }
                
                if self.insertInitialLoadMessages(messageDictionary: item) {
                  JSQSystemSoundPlayer.jsq_playMessageReceivedSound()
                }
                
                self.finishReceivingMessage()
              }
            }
          }
        }
      }
    })
  }
  // MARK: - Insert Messages
  func insertMessages() {
    maxMessageNumber = loadedMessages.count - loadedMessagesCount
    minMessageNumber = maxMessageNumber - kNUMBEROFMESSAGES
    
    if minMessageNumber < 0 {
      minMessageNumber = 0
    }
    
    for i in minMessageNumber ..< maxMessageNumber {
      let messageDictionary = loadedMessages[i]
      
      // insert message
      insertInitialLoadMessages(messageDictionary: messageDictionary)
      loadedMessagesCount += 1
    }
    self.showLoadEarlierMessagesHeader = (loadedMessagesCount != loadedMessages.count)
  }
  
  func insertInitialLoadMessages(messageDictionary: NSDictionary) -> Bool {
    let incomingMessage = IncomingMessage(collectionView: collectionView)
    guard let senderId = messageDictionary[kSENDERID] as? String else { return false }
    if senderId != FUser.currentId() {
      OutgoingMessage.updateMessage(withId: messageDictionary[kMESSAGEID] as! String, chatRoomId: chatRoomId ?? "", memberIds: memberIds ?? [])
    }
    guard let chatRoomId = chatRoomId, let message = incomingMessage.createMessage(messageDictionary: messageDictionary, chatroomId: chatRoomId) else { return false }
    
    objectMessages.append(messageDictionary)
    messages.append(message)
    return isIncoming(messageDictionary: messageDictionary)
  }
  
  // MARK: - Helper functions
  func readTimeFrom(dateString: String) -> String {
    let date = dateFormatter().date(from: dateString)
    let currentDateFormat = dateFormatter()
    currentDateFormat.dateFormat = "HH:mm"
    return currentDateFormat.string(from: date ?? Date())
  }
  
  func removeBadMessages(allMessages: [NSDictionary]) -> [NSDictionary] {
    var tempMessages = allMessages
    for message in tempMessages {
      if let type = message[kTYPE] as? String {
        if !legitTypes.contains(type) {
          // remove the message
          if let index = tempMessages.index(of: message) {
            tempMessages.remove(at: index)
          }
        }
      }
    }
    return tempMessages
  }
  
  func isIncoming(messageDictionary: NSDictionary) -> Bool {
    guard let messageSenderId = messageDictionary[kSENDERID] as? String else { return false }
    if FUser.currentId() == messageSenderId {
      return false
    } else {
      return true
    }
  }
}

//MARK: - UIImagePickerController Delegate

extension DirectMessageViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    let video = info[UIImagePickerController.InfoKey.mediaURL] as? NSURL
    
    let picture = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
    
    sendMessage(text: nil, date: Date(), picture: picture, location: nil, video: video, audio: nil)
    
    picker.dismiss(animated: true, completion: nil)
  }
}

//MARK: - IQAudio Recorder Controller Delegate
extension DirectMessageViewController: IQAudioRecorderViewControllerDelegate {
  func audioRecorderController(_ controller: IQAudioRecorderViewController, didFinishWithAudioAtPath filePath: String) {
    controller.dismiss(animated: true, completion: nil)
    self.sendMessage(text: nil, date: Date(), picture: nil, location: nil, video: nil, audio: filePath)
  }
  
  func audioRecorderControllerDidCancel(_ controller: IQAudioRecorderViewController) {
    controller.dismiss(animated: true, completion: nil)
  }
}

// location function
extension DirectMessageViewController {
  
  //MARK: - Location access
  func haveAccessToUserLocation() -> Bool {
    if appDelegate.locationManager != nil {
      return true
    }else {
      ProgressHUD.showError("Please give access location in Settings")
      return false
    }
  }
}
