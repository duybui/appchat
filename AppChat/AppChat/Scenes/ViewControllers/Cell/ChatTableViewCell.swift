//
//  ChatTableViewCell.swift
//  WhosWin
//
//  Created by Duy Bui on 3/3/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit

protocol ChatTableViewCellDelegate: class {
  func didTapAvatarImage(indexPath: IndexPath?)
}

class ChatTableViewCell: UITableViewCell {
  
  @IBOutlet weak var userImageView: UIImageView!
  @IBOutlet weak var newMessageLabel: UILabel!
  @IBOutlet weak var lastSyncDateLabel: UILabel!
  @IBOutlet weak var lastMessageLabel: UILabel!
  @IBOutlet weak var fullnameLabel: UILabel!
  let tapGesture = UITapGestureRecognizer()
  var indexPath: IndexPath?
  weak var delegate: ChatTableViewCellDelegate?
  override func awakeFromNib() {
    super.awakeFromNib()
    
    tapGesture.addTarget(self, action: #selector(self.avatarTap))
    userImageView.isUserInteractionEnabled = true
    userImageView.addGestureRecognizer(tapGesture)
  }
  func generateCell(recentChat: NSDictionary, indexPath: IndexPath?) {
    self.indexPath = indexPath
    self.fullnameLabel.text = recentChat[kWITHUSERFULLNAME] as? String
    self.lastMessageLabel.text = recentChat[kLASTMESSAGE] as? String
    self.newMessageLabel.text = recentChat[kCOUNTER] as? String
    if let avatarString = recentChat[kAVATAR] as? String {
      imageFromData(pictureData: avatarString) { (avatarImage) in
        if let avatarImage = avatarImage {
          self.userImageView.image = avatarImage.circleMasked
        }
      }
    }
    if let count = recentChat[kCOUNTER] as? Int, count != 0 {
      self.newMessageLabel.text = "\(count)"
      self.newMessageLabel.isHidden = false
    } else {
      self.newMessageLabel.isHidden = true
    }
    
    var date: Date?
    if let created = recentChat[kDATE] as? String {
      if created.count != 14 {
        date = Date()
      } else {
        date = dateFormatter().date(from: created)
      }
    } else {
      date = Date()
    }
    self.lastSyncDateLabel.text = timeElapsed(date: date)
  }
  
  @objc private func avatarTap() {
    delegate?.didTapAvatarImage(indexPath: indexPath)
  }
}
