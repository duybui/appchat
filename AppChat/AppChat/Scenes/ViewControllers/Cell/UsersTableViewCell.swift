//
//  UsersTableViewCell.swift
//  AppChat
//
//  Created by Duy Bui on 2/17/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit
protocol UserTableViewCellDelegate: class {
  func didTapAvatarImage(indexPath: IndexPath)
}

class UsersTableViewCell: UITableViewCell {

  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var userImageView: UIImageView!
  
  var indexPath: IndexPath!
  let tapGestureRecognizer = UITapGestureRecognizer()
  weak var delegate: UserTableViewCellDelegate?
  override func awakeFromNib() {
    super.awakeFromNib()
    
    tapGestureRecognizer.addTarget(self, action: #selector(self.avatarTap))
    userImageView.isUserInteractionEnabled = true
    userImageView.addGestureRecognizer(tapGestureRecognizer)
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
  }
  
  func generateCellWith(fUser: FUser, indexPath: IndexPath) {
    self.indexPath = indexPath
    self.nameLabel.text = fUser.fullname
    if fUser.avatar != "" {
      imageFromData(pictureData: fUser.avatar) { (avatarImage) in
        if avatarImage != nil {
          self.userImageView.image = avatarImage!.circleMasked
        }
      }
    }
  }
  
  @objc func avatarTap() {
    print("avatar tap at \(indexPath)")
    delegate?.didTapAvatarImage(indexPath: indexPath)
  }
}
