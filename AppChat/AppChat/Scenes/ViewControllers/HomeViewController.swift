//
//  HomeViewController.swift
//  AppChat
//
//  Created by Duy Bui on 2/6/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
  @IBAction func chat(_ sender: Any) {
    guard let navigationController = Application.shared.navigator, let vc = UIStoryboard.Screen.users else { return }
    navigationController.pushViewController(vc, animated: true)
  }
  
  @IBAction func logout(_ sender: Any) {
    FUser.logOutCurrentUser { (success) in
      if success {
        Application.shared.startWalkthroughScreen()
      }
    }
  }
  /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
