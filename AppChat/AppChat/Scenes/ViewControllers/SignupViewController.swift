//
//  SignupViewController.swift
//  AppChat
//
//  Created by Duy Bui on 2/6/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit
import ProgressHUD

class SignupViewController: BaseViewController {

  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var repeatPasswordTextField: UITextField!
  
  @IBOutlet weak var firstNameTextField: UITextField!
  
  @IBOutlet weak var lastNameTextField: UITextField!
  
  @IBOutlet weak var clubTextField: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()

    setupUINavigationBar()
  }
  
  @IBAction func didTapOnSignupButton(_ sender: Any) {
    
    dismissKeyboard()
    if emailTextField.text != "" && passwordTextField.text != "" && repeatPasswordTextField.text != "" {
      
      if passwordTextField.text == repeatPasswordTextField.text {
        signup()
      } else {
        ProgressHUD.showError("Password don't match")
      }
    } else {
      ProgressHUD.showError("All fields are missing")
    }
  }
  
  private func registerUser() {
    let fullName = firstNameTextField.text! + " " + lastNameTextField.text!
    var tempDictionary: Dictionary = [
      kFIRSTNAME: firstNameTextField.text!,
      kLASTNAME: lastNameTextField.text!,
      kFULLNAME: fullName,
      kFAVORITECLUB: clubTextField.text!
      ] as [String : Any]
    
    imageFromInitials(firstName: firstNameTextField.text!, lastName: lastNameTextField.text!) { (avatar) in
      let avatarIMG = avatar.jpegData(compressionQuality: 0.7)
      let serverAvatar = avatarIMG?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
      tempDictionary[kAVATAR] = serverAvatar
      self.finishRegistration(withValue: tempDictionary)
    }
  }
  
  private func signup() {
    ProgressHUD.show("Signup...")
    FUser.registerUserWith(email: emailTextField.text!, password: passwordTextField.text!) { (error) in
      if error != nil {
        ProgressHUD.dismiss()
        ProgressHUD.showError(error!.localizedDescription)
        return
      }
      ProgressHUD.dismiss()
      self.registerUser()
    }
  }
  
  private func finishRegistration(withValue: [String: Any]) {
    updateCurrentUserInFirestore(withValues: withValue) { (error) in
      if error != nil {
        DispatchQueue.main.async {
          ProgressHUD.showError(error!.localizedDescription)
        }
        return
      }
      
      /// go to app
      self.goToApp()
    }
  }
  
  private func goToApp() {
    guard let navigationController = Application.shared.navigator, let vc = UIStoryboard.Screen.tabBar else { return }
    navigationController.pushViewController(vc, animated: true)
  }
  private func cleanTextField() {
    emailTextField.text = ""
    passwordTextField.text = ""
    repeatPasswordTextField.text = ""
  }
}
