//
//  SelectClubViewController.swift
//  AppChat
//
//  Created by Duy Bui on 2/23/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit

class SelectClubViewController: UIViewController {
  var list = ["a1", "a2", "a3", "a4", "a5"]
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var backgroundImage: UIImageView!
  let flowLayout = ZoomAndSnapFlowLayout()
  let identifierCell = "SelectionCollectionViewCell"
  var itemSize = 100
  override func viewDidLoad() {
    super.viewDidLoad()
    
    collectionView.decelerationRate = .normal // uncomment if necessary
    itemSize = Helper.isLessThanOrEqualIP5Size() ? 200 : 250
    flowLayout.itemSize = CGSize(width: itemSize, height: itemSize)
    collectionView.collectionViewLayout = flowLayout
    let nib = UINib(nibName: identifierCell, bundle: nil)
    collectionView.register(nib, forCellWithReuseIdentifier: identifierCell)
    backgroundImage.image = UIImage(named: list[0])
  }
}

extension SelectClubViewController: UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return list.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifierCell, for: indexPath) as? SelectionCollectionViewCell else { return UICollectionViewCell() }
    cell.selectedImage = list[indexPath.row]
    return cell
  }
}

extension SelectClubViewController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: collectionView.bounds.width - 60, height: collectionView.bounds.height)
  }
}

extension SelectClubViewController: UIScrollViewDelegate {
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    var visibleRect = CGRect()
    visibleRect.origin = collectionView.contentOffset
    visibleRect.size = collectionView.bounds.size
    let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
    guard let indexPath = collectionView.indexPathForItem(at: visiblePoint) else { return }
    backgroundImage.image = UIImage(named: list[indexPath.row])
  }
}
