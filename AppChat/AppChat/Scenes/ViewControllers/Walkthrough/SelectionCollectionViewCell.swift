//
//  SelectionCollectionViewCell.swift
//  AppChat
//
//  Created by Duy Bui on 2/23/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit

class SelectionCollectionViewCell: UICollectionViewCell {

  @IBOutlet weak var imageView: UIImageView!
  override func awakeFromNib() {
      super.awakeFromNib()
  }
  var selectedImage: String = "" {
    didSet {
      imageView.image = UIImage(named: selectedImage)
      imageView.contentMode = .scaleAspectFill
    }
  }
}
