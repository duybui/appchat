//
//  WalkthroughViewController.swift
//  AppChat
//
//  Created by Duy Bui on 2/6/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit

class WalkthroughViewController: BaseViewController {

  @IBOutlet weak var nextButton: UIButton!
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var pageControl: UIPageControl!
  
  @IBOutlet weak var descriptionLabel: UILabel!
  var list = ["b1", "b2", "b3"]
  var descriptionList = ["Connect with friends, clubs and fans.\n Create talking points, polls and sort out that next trip to the pub.", "Track news, results, scores, stats fixtures, videos and key dates of all your favourite teams.\n All Leagues. All Competitions.", "React to the score, the weekends big game, or weigh in the latest transfer.\n It’s all football. It’s all here."]
  let identifierCell = "SelectionCollectionViewCell"
  private let infinitiesSize = 1000
  private let numberOfScreen = 3
  private var timer: Timer?
  private var timeInterval: Double = 5.0
  
  override func viewDidLoad() {
    super.viewDidLoad()

    let nib = UINib(nibName: identifierCell, bundle: nil)
    collectionView.register(nib, forCellWithReuseIdentifier: identifierCell)
    pageControl.numberOfPages = numberOfScreen
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    let startIndexPath = IndexPath(row: 499, section: 0)
    collectionView.scrollToItem(at: startIndexPath, at: .centeredHorizontally, animated: false)
    runTimer()
    pageControl.currentPage = 0
  }
  
  @IBAction func didTapOnLoginButton(_ sender: Any) {
    guard let navigationController = Application.shared.navigator, let vc = UIStoryboard.Screen.login else { return }
    navigationController.pushViewController(vc, animated: true)
  }
  
  @IBAction func didTapOnSignInButton(_ sender: Any) {
    guard let navigationController = Application.shared.navigator, let vc = UIStoryboard.Screen.signup else { return }
    navigationController.pushViewController(vc, animated: true)
  }
}

extension WalkthroughViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 1000
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifierCell, for: indexPath) as? SelectionCollectionViewCell else { return UICollectionViewCell() }
    cell.selectedImage = list[indexPath.row % numberOfScreen]
    descriptionLabel.text = descriptionList[indexPath.row % numberOfScreen]
    return cell
  }
}

extension WalkthroughViewController: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
  }
}

// timer
extension WalkthroughViewController {
  private func runTimer() {
    
    resetTimer()
    timer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(slideToNextPage), userInfo: nil, repeats: true)
  }
  
  private func resetTimer() {
    
    guard let timer = timer else { return }
    timer.invalidate()
  }
  
  @objc func slideToNextPage() {
    
    let nextPage = (self.pageControl.currentPage + 1) == numberOfScreen ? 0 : (self.pageControl.currentPage + 1)
    self.pageControl.currentPage = nextPage
    
    let nextItem = IndexPath(item: getCurrentIndex().item + 1, section: 0)
    self.collectionView.scrollToItem(at: nextItem, at: .left, animated: true)
  }
  
  private func getCurrentIndex() -> IndexPath {
    
    let visibleItem: NSArray = self.collectionView.indexPathsForVisibleItems as NSArray
    guard let currentItem: IndexPath = visibleItem.object(at: 0) as? IndexPath else {
      return IndexPath()
    }
    return currentItem
  }
}

extension WalkthroughViewController: UIScrollViewDelegate {
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    
    let xOffset = self.collectionView.contentOffset.x
    let width = self.collectionView.bounds.size.width
    let index = Int(xOffset/width) % numberOfScreen
    pageControl.currentPage = index
    
    resetTimer()
    runTimer()
  }
}
