//
//  UsersViewController.swift
//  AppChat
//
//  Created by Duy Bui on 2/17/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit
import ProgressHUD
import Firebase
import RxCocoa
import RxSwift

class UsersViewController: BaseViewController {
  
  @IBOutlet weak var searchTextField: UITextField!
  @IBOutlet weak var userSegment: UISegmentedControl!
  @IBOutlet weak var userTableView: UITableView!
  var allUsers: [FUser] = []
  var filteredUser: [FUser] = []
  let disposeBag = DisposeBag()
  var isSearch = false
  let cellIdentifier = "UsersTableViewCell"
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUINavigationBar()
    let nib = UINib(nibName: cellIdentifier, bundle: nil)
    userTableView.register(nib, forCellReuseIdentifier: cellIdentifier)
    loadUsers(filter: kFAVORITECLUB)
    searchTextField.rx.text.orEmpty.asObservable().subscribe(onNext: { [weak self] (value) in
      guard let self = self else { return }
      self.isSearch = !value.isEmpty
      self.filterContentForSearchText(searchText: value.lowercased())
    }).disposed(by: disposeBag)
  }
  
  @IBAction func segmentAction(_ sender: UISegmentedControl) {
    switch sender.selectedSegmentIndex {
    case 0:
      loadUsers(filter: kFAVORITECLUB)
    case 1:
      loadUsers(filter: "")
    default:
      break
    }
  }
}

extension UsersViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return isSearch ? filteredUser.count: allUsers.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! UsersTableViewCell
    cell.generateCellWith(fUser: isSearch ? filteredUser[indexPath.row]: allUsers[indexPath.row], indexPath: indexPath)
    cell.delegate = self
    return cell
  }
}

extension UsersViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let anotherUser = isSearch ? filteredUser[indexPath.row]: allUsers[indexPath.row]
    guard let currentUser = FUser.currentUser() else { return }
    _ = ChatService.shared.startPrivateChat(firstUser: currentUser, secondUser: anotherUser)
  }
}

extension UsersViewController {
  func filterContentForSearchText(searchText: String) {
    filteredUser = allUsers.filter({ (user) -> Bool in
      return user.fullname.lowercased().contains(searchText)
    })
    userTableView.reloadData()
  }
  
  func loadUsers(filter: String) {
    ProgressHUD.show()
    var query: Query!
    switch filter {
    case kFAVORITECLUB:
      query = reference(.User).whereField(kFAVORITECLUB, isEqualTo: FUser.currentUser()!.favoriteClub).order(by: kFIRSTNAME, descending: false)
    case kCOUNTRY:
      query = reference(.User).whereField(kCOUNTRY, isEqualTo: FUser.currentUser()!.country).order(by: kFIRSTNAME, descending: false)
    default:
      query = reference(.User)
    }
    query.getDocuments { (snapshot, error) in
      self.allUsers = []
      if error != nil {
        print(error!.localizedDescription)
        ProgressHUD.dismiss()
        self.userTableView.reloadData()
        return
      }
      
      guard let snapShot = snapshot else {
        ProgressHUD.dismiss()
        return
      }
      if !snapShot.isEmpty {
        for eachUserDictionary in snapShot.documents {
          let userDictionary = eachUserDictionary.data() as NSDictionary
          let fUser = FUser(_dictionary: userDictionary)
          if fUser.objectId != FUser.currentId() {
            self.allUsers.append(fUser)
          }
        }
        // split to groups
      }
      self.userTableView.reloadData()
      ProgressHUD.dismiss()
    }
  }
}

extension UsersViewController: UserTableViewCellDelegate {
  func didTapAvatarImage(indexPath: IndexPath) {
    guard let profile = UIStoryboard.Screen.profile else { return }
    profile.user = isSearch ? filteredUser[indexPath.row]: allUsers[indexPath.row]
    self.navigationController?.pushViewController(profile, animated: true)
  }
}
