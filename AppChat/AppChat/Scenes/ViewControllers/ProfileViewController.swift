//
//  ProfileViewController.swift
//  WhosWin
//
//  Created by Duy Bui on 3/3/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var fullNameLabel: UILabel!
  @IBOutlet weak var phoneLabel: UILabel!
  @IBOutlet weak var blockUserButton: UIButton!
  @IBOutlet weak var callButton: UIButton!
  @IBOutlet weak var messageButton: UIButton!
  var user: FUser?
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUI()
  }
  
  @IBAction func didTapOnCallButton(_ sender: Any) {
    
  }
  
  @IBAction func didTapOnMessageButton(_ sender: Any) {
    
  }
  
  @IBAction func didTapOnBlockButton(_ sender: Any) {
    guard var currentBlockedIds = FUser.currentUser()?.blockedUsers, let user = user else { return }
    if currentBlockedIds.contains(user.objectId), let indexPath = currentBlockedIds.index(of: user.objectId) {
      currentBlockedIds.remove(at: indexPath)
    } else {
      currentBlockedIds.append(user.objectId)
    }
    
    updateCurrentUserInFirestore(withValues: [kBLOCKEDUSERID: currentBlockedIds]) { (error) in
      if let error = error {
        print("Error updating user \(error.localizedDescription)")
        return
      }
      self.updateBlockStatus()
    }
  }
  
  private func setupUI() {
    if let user = user {
      fullNameLabel.text = user.fullname
      phoneLabel.text = user.phoneNumber
      updateBlockStatus()
      imageFromData(pictureData: user.avatar) { (avatarImage) in
        if let avatarImage = avatarImage {
          self.imageView.image = avatarImage.circleMasked
        }
      }
    }
  }
  
  private func updateBlockStatus() {
    if user?.objectId != FUser.currentId() {
      blockUserButton.isHidden = false
      callButton.isHidden = false
      messageButton.isHidden = false
    } else {
      blockUserButton.isHidden = true
      callButton.isHidden = true
      messageButton.isHidden = true
    }
    
    if (FUser.currentUser()?.blockedUsers.contains(user!.objectId) ?? false) {
      blockUserButton.setTitle("Unblock user", for: .normal)
    } else {
      blockUserButton.setTitle("Block user", for: .normal)
    }
  }
}
