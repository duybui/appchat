//
//  LoginViewController.swift
//  AppChat
//
//  Created by Duy Bui on 2/6/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit
import ProgressHUD

class LoginViewController: BaseViewController {

  @IBOutlet weak var emailTextfield: UITextField!
  @IBOutlet weak var passwordTextfield: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()

    setupUINavigationBar()
  }

  @IBAction func didTapOnLoginButton(_ sender: Any) {
    dismissKeyboard()
    if emailTextfield.text != "" && passwordTextfield.text != "" {
      loginUser()
    } else {
      ProgressHUD.showError("Email and password is missing")
    }
  }
  
  private func loginUser() {
    ProgressHUD.show("Login...")
    FUser.loginUserWith(email: emailTextfield.text!, password: passwordTextfield.text!) { (error) in
      if error != nil {
        ProgressHUD.showError(error!.localizedDescription)
        return
      }
      ProgressHUD.dismiss()
        guard let navigationController = Application.shared.navigator, let vc = UIStoryboard.Screen.tabBar else { return }
        navigationController.pushViewController(vc, animated: true)
    }
  }
  
  private func cleanTextField() {
    emailTextfield.text = ""
    passwordTextfield.text = ""
  }
}
