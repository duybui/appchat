//
//  AudioViewController.swift
//  WhosWin
//
//  Created by Duy Bui on 3/30/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import Foundation
import IQAudioRecorderController

class AudioViewController {
  var delegate: IQAudioRecorderViewControllerDelegate
  
  init(delegate_: IQAudioRecorderViewControllerDelegate) {
    delegate = delegate_
  }
  
  func presentAudioRecorder(target: UIViewController) {
    let controller = IQAudioRecorderViewController()
    controller.title = "Record"
    controller.delegate = delegate
    controller.maximumRecordDuration = kAUDIOMAXDURATION
    controller.allowCropping = true
    
    target.presentBlurredAudioRecorderViewControllerAnimated(controller)
  }
}

