//
//  ChatViewController.swift
//  WhosWin
//
//  Created by Duy Bui on 3/3/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit
import FirebaseFirestore
import RxSwift
import RxCocoa

class ChatViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var searchTextField: UITextField!
  
  let cellIdentifier = "ChatTableViewCell"
  var recentChats: [NSDictionary] = []
  var filterChats: [NSDictionary] = []
  var isSearch = false
  
  var recentListener: ListenerRegistration?
  let disposeBag = DisposeBag()
  override func viewDidLoad() {
    super.viewDidLoad()
    let nib = UINib(nibName: cellIdentifier, bundle: nil)
    tableView.register(nib, forCellReuseIdentifier: cellIdentifier)
    searchTextField.rx.text.orEmpty.asObservable().subscribe(onNext: { [weak self] (value) in
      guard let self = self else { return }
      self.isSearch = !value.isEmpty
      self.filterContentForSearchText(searchText: value.lowercased())
    }).disposed(by: disposeBag)
    loadRecentChats()
  }
  
  @IBAction func didTapOnNewGroupButton(_ sender: Any) {
  }
}
extension ChatViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return isSearch ? filterChats.count: recentChats.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ChatTableViewCell else { return UITableViewCell() }
    let recent = isSearch ? filterChats[indexPath.row]: recentChats[indexPath.row]
    cell.generateCell(recentChat: recent, indexPath: indexPath)
    cell.delegate = self
    return cell
  }
}

extension ChatViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }
  
  func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    var tempRecent: NSDictionary?
    tempRecent = isSearch ? filterChats[indexPath.row]: recentChats[indexPath.row]
    var muteTitle = "Unmute"
    var mute = false
    
    if let tempRecentMemberToPush = tempRecent?[kMEMBERSTOPUSH] as? [String], tempRecentMemberToPush.contains(FUser.currentId()) {
      muteTitle = "Mute"
      mute = true
    }
    
    let deleteAction = UITableViewRowAction(style: .default, title: "Delete") { [weak self] (action, indexPath) in
      guard let self = self else { return }
      _ = self.isSearch ? self.filterChats.remove(at: indexPath.row): self.recentChats.remove(at: indexPath.row)
      ChatService.shared.deleteRecentChat(recentChatDictionary: tempRecent ?? [:])
      self.tableView.reloadData()
    }
    
    let muteAction = UITableViewRowAction(style: .default, title: muteTitle) { (action, indexPath) in
      
    }
    muteAction.backgroundColor = .blue
    return [deleteAction, muteAction]
  }
}

extension ChatViewController {
  
  func loadRecentChats() {
    recentListener = reference(.Recent).whereField(kUSERID, isEqualTo: FUser.currentId()).addSnapshotListener({ (snapshot, error) in
      guard let snapshot = snapshot else { return }
      self.recentChats = []
      if !snapshot.isEmpty {
        guard let sorted = ((dictionaryFromSnapshots(snapshots: snapshot.documents)) as NSArray).sortedArray(using: [NSSortDescriptor(key: kDATE, ascending: false)]) as? [NSDictionary] else { return }
        for recent in sorted {
          if let message = recent[kLASTMESSAGE] as? String, !message.isEmpty &&
            recent[kCHATROOMID] != nil && recent[kRECENTID] != nil {
            self.recentChats.append(recent)
          }
        }
        self.tableView.reloadData()
      }
    })
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    var recent: NSDictionary?
    recent = isSearch ? filterChats[indexPath.row]: recentChats[indexPath.row]
    
    //restart chat
    ChatService.shared.restartRecentChat(recent: recent ?? [:])
    
    //show chat view
    let directMessageVC = DirectMessageViewController()
    directMessageVC.hidesBottomBarWhenPushed = true
    directMessageVC.titleName = recent?[kWITHUSERFULLNAME] as? String
    directMessageVC.memberIds = recent?[kMEMBERS] as? [String]
    directMessageVC.membersToPush = recent?[kMEMBERSTOPUSH] as? [String]
    directMessageVC.chatRoomId = recent?[kCHATROOMID] as? String
    directMessageVC.isGroup = (recent?[kCHATROOMID] as? String ?? "") == kGROUP
    self.navigationController?.pushViewController(directMessageVC, animated: true)
  }
}

extension ChatViewController: ChatTableViewCellDelegate {
  func didTapAvatarImage(indexPath: IndexPath?) {
    guard let indexPath = indexPath else { return }
    let recentChat = recentChats[indexPath.row]
    if let typeRecentChat = recentChat[kTYPE] as? String, typeRecentChat == kPRIVATE,
      let userId = recentChat[kWITHUSERUSERID] as? String {
      reference(.User).document(userId).getDocument { (snapshot, error) in
        guard let snapshot = snapshot else { return }
        if snapshot.exists {
          let userDictionary = snapshot.data() as NSDictionary?
          let tempUser = FUser(_dictionary: userDictionary ?? [:])
          self.showUserProfile(user: tempUser)
        }
      }
    }
  }
  
  func showUserProfile(user: FUser) {
    guard let profile = UIStoryboard.Screen.profile else { return }
    profile.user = user
    self.navigationController?.pushViewController(profile, animated: true)
  }
  
  func filterContentForSearchText(searchText: String) {
    filterChats = recentChats.filter({ (recentChat) -> Bool in
      guard let recentChat = recentChat[kWITHUSERFULLNAME] as? String else { return false }
      return recentChat.lowercased().contains(searchText)
    })
    tableView.reloadData()
  }
}
