//
//  Constant.swift
//  AppChat
//
//  Created by Duy Bui on 2/6/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import Foundation
import UIKit

typealias Action = () -> Void
struct Constant {
  
  struct Size {
    
    static let activityIndicator = CGSize(width: 50, height: 50)
  }
  
  struct Number {
    
    static let hideIndicatorDelay = 0.1
    static let defaultAnimationDuration = 0.25
  }
  
  struct TagViewPopup {
    
    static let oneOption = -111
    static let twoOptions = -112
    static let firstLogin = -113
    static let sendMoneyNumberOrEmailDialog = -114
    static let moneyRequestReceivedDialog = -115
  }
}

// MARK: - AlertMessage
struct AlertMessage {
  
  var title: String
  var content: String
  var textButton: String
  var contentAlignment: NSTextAlignment
  var highLightContent: String
  
  init(title: String, content: String, textButton: String, contentAlignment: NSTextAlignment = .center, highLightContent: String = "") {
    self.title = title
    self.content = content
    self.textButton = textButton
    self.contentAlignment = contentAlignment
    self.highLightContent = highLightContent
  }
}

typealias Parameters = [String:Any]

enum Result <T , E: Error> {
  case success(T)
  case failure(E)
}

