//
//  CommonEnum.swift
//  AppChat
//
//  Created by Duy Bui on 2/6/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import Foundation

// MARK: - Navigation bar
enum NavBarTitle: String {
  case splash = "Splash"
  case signup = "Sign Up"
  case login =  "Log In"
  
  var name: String {
    return rawValue
  }
}

enum NavBarIcon: String {
  case back = "backButton"
  case pageMenu = "paga-iphone-icon-hamburger"
  case warmPageMenu = "paga-iphone-icon-warm-hamburger"
  case whiteBack = "paga-iphone-white-back"
  var name: String {
    return rawValue
  }
}
