//
//  ChatService.swift
//  WhosWin
//
//  Created by Duy Bui on 3/3/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import Foundation
class ChatService {
  private init() {}
  static let shared = ChatService()
  func startPrivateChat(firstUser: FUser, secondUser: FUser) -> String {
    let firstUserId = firstUser.objectId
    let secondUserId = secondUser.objectId
    
    var chatRoomId = ""
    
    let value = firstUserId.compare(secondUserId).rawValue
    chatRoomId = value < 0 ? (firstUserId + secondUserId): (secondUserId + firstUserId)
    let members = [firstUserId, secondUserId]
    createRecent(members: members, chatRoomId: chatRoomId, withUserName: "", type: kPRIVATE, users: [firstUser, secondUser], avatarOfGroup: nil)
    return chatRoomId
  }
  
  private func createRecent(members: [String], chatRoomId: String, withUserName: String, type: String, users: [FUser]?, avatarOfGroup: String?) {
    var tempMembers = members
    reference(.Recent).whereField(kCHATROOMID, isEqualTo: chatRoomId).getDocuments { [weak self] (snapshot, error) in
      
      guard let self = self, let snapshot = snapshot else { return }
      if !snapshot.isEmpty {
        for recent in snapshot.documents {
          let currentRecent = recent.data() as NSDictionary
          if let currentUserId = currentRecent[kUSERID] as? String,
            let indexPath = tempMembers.index(of: currentUserId) {
            if tempMembers.contains(currentUserId) {
              tempMembers.remove(at: indexPath)
            }
          }
        }
      }
      
      for userId in tempMembers {
        self.createRecentItem(userId: userId, chatRoomId: chatRoomId, members: members, withUserName: withUserName, type: type, users: users, avatarOfGroup: avatarOfGroup)
      }
    }
  }
  
  private func createRecentItem(userId: String, chatRoomId: String, members: [String], withUserName: String, type: String, users: [FUser]?, avatarOfGroup: String?) {
    let localReference = reference(.Recent).document()
    let recentId = localReference.documentID
    let date = dateFormatter().string(from: Date())
    var recent: [String: Any]!
    if type == kPRIVATE {
      // private
      var withUser: FUser?
      if let users = users, users.count > 0 {
        if userId == FUser.currentId() {
          withUser = users.last!
        } else {
          withUser = users.first!
        }
      }
      guard let withUnwrapUser = withUser else { return }
      recent = [ kRECENTID: recentId, kUSERID: userId, kCHATROOMID: chatRoomId, kMEMBERS: members,
                 kMEMBERSTOPUSH: members, kWITHUSERFULLNAME: withUnwrapUser.fullname, kWITHUSERUSERID: withUnwrapUser.objectId, kLASTMESSAGE: "",
                 kCOUNTER: 0, kDATE: date, kTYPE: type, kAVATAR: withUnwrapUser.avatar
      ] as [String: Any]
    } else {
      // group
      recent = [ kRECENTID: recentId, kUSERID: userId, kCHATROOMID: chatRoomId, kMEMBERS: members,
                 kMEMBERSTOPUSH: members, kWITHUSERFULLNAME: withUserName, kLASTMESSAGE: "",
                 kCOUNTER: 0, kDATE: date, kTYPE: type, kAVATAR: (avatarOfGroup ?? "")
        ] as [String: Any]
    }
    
    // save recent chat
    localReference.setData(recent)
  }
  
  func deleteRecentChat(recentChatDictionary: NSDictionary) {
    if let recentId = recentChatDictionary[kRECENTID] as? String {
      reference(.Recent).document(recentId).delete()
    }
  }
  
  func restartRecentChat(recent: NSDictionary) {
    if let recentId = recent[kTYPE] as? String, recentId == kPRIVATE {
      guard let member = recent[kMEMBERSTOPUSH] as? [String],
      let chatRoomId = recent[kCHATROOMID] as? String,
      let currentUser = FUser.currentUser() else { return }
      createRecent(members: member, chatRoomId: chatRoomId, withUserName: currentUser.firstname, type: kPRIVATE, users: [currentUser], avatarOfGroup: nil)
    }
    
    if let recentId = recent[kTYPE] as? String, recentId == kGROUP {
      guard let member = recent[kMEMBERSTOPUSH] as? [String],
        let chatRoomId = recent[kCHATROOMID] as? String,
        let userName = recent[kWITHUSERUSERNAME] as? String,
        let avatar = recent[kAVATAR] as? String,
        let currentUser = FUser.currentUser() else { return }
      createRecent(members: member, chatRoomId: chatRoomId, withUserName: userName, type: kGROUP, users: [currentUser], avatarOfGroup: avatar)
    }
  }
}
