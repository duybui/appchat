//
//  Helper.swift
//
//  Created by Duy Bui on 9/29/18.
//  Copyright © 2018 DuyBui. All rights reserved.
//

import UIKit

class Helper {
  
  // MARK: - Version Iphone
  static func isLessThanOrEqualIP5Size() -> Bool {
    return UIScreen.main.bounds.size.height <= 568
  }
  // this function is not used at anywhere else.
  static func isIP7Size() -> Bool {
    return UIScreen.main.bounds.size.height == 667
  }
  
  static func isGreaterThanIP7Size() -> Bool {
    return UIScreen.main.bounds.size.height > 667
  }
  
  static func isiPhoneX() -> Bool {
    return UIScreen.main.bounds.size.height == 812
  }
  
  static func isIP4Size() -> Bool {
    return UIScreen.main.bounds.size.height == 480
  }
}
