//
//  UIButton+RxSwift+Ext.swift
//
//  Created by Duy Bui on 10/9/18.
//  Copyright © 2018 DuyBui. All rights reserved.
//

import RxSwift
import RxCocoa

extension Reactive where Base: UIButton {
  
  var isEnableAlpha: Binder<Bool> {
    return Binder.init(base, binding: { (button, valid) in
      button.isEnabled = valid
      button.alpha = valid ? 1.0 : 0.5
    })
  }
}
