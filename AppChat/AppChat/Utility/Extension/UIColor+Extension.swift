//
//  UIColor+Extension.swift
//  AppChat
//
//  Created by Duy Bui on 2/6/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit

extension UIColor {
  
  struct InApp {
    
    static let blue = UIColor(red: 0 / 255, green: 193 / 255, blue: 232 / 255, alpha: 1.0)
    static let grey = UIColor(red: 123.0 / 255.0, green: 123.0 / 255.0, blue: 122.0 / 255.0, alpha: 1.0)
    static let purpleBrown = UIColor(red: 35.0 / 255.0, green: 31.0 / 255.0, blue: 32.0 / 255.0, alpha: 1.0)
  }
}
