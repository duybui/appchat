//
//  UIStoryboard+Extension.swift
//  AppChat
//
//  Created by Duy Bui on 2/6/19.
//  Copyright © 2019 DuyBui. All rights reserved.
//

import UIKit

extension UIStoryboard {
  
  static var main: UIStoryboard {
    return UIStoryboard(name: "Main", bundle: nil)
  }
  
  static var walkthrough: UIStoryboard {
    return UIStoryboard(name: "Walkthrough", bundle: nil)
  }
  
  static var login: UIStoryboard {
    return UIStoryboard(name: "Login", bundle: nil)
  }
  
  static var signup: UIStoryboard {
    return UIStoryboard(name: "Signup", bundle: nil)
  }
  
  static var home: UIStoryboard {
    return UIStoryboard(name: "Home", bundle: nil)
  }
  
  static var tabBar: UIStoryboard {
    return UIStoryboard(name: "TabBar", bundle: nil)
  }
  
  static var settings: UIStoryboard {
    return UIStoryboard(name: "Settings", bundle: nil)
  }
  
  static var users: UIStoryboard {
    return UIStoryboard(name: "Users", bundle: nil)
  }
  
  static var profile: UIStoryboard {
    return UIStoryboard(name: "Profile", bundle: nil)
  }
  
  static var chat: UIStoryboard {
    return UIStoryboard(name: "Chat", bundle: nil)
  }
  
  struct Screen {
    static var navigator: UINavigationController? {
      return UIStoryboard.main.instantiateInitialViewController() as? UINavigationController
    }
    
    static var splash: SplashViewController? {
      return UIStoryboard.main.instantiateViewController(withIdentifier: "SplashViewController") as? SplashViewController
    }
    
    static var walkthrough: WalkthroughViewController? {
      return UIStoryboard.walkthrough.instantiateViewController(withIdentifier: "WalkthroughViewController") as? WalkthroughViewController
    }
    
    static var selectClub: SelectClubViewController? {
      return UIStoryboard.walkthrough.instantiateViewController(withIdentifier: "SelectClubViewController") as? SelectClubViewController
    }
    
    static var login: LoginViewController? {
      return UIStoryboard.login.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
    }
    
    static var signup: SignupViewController? {
      return UIStoryboard.signup.instantiateViewController(withIdentifier: "SignupViewController") as? SignupViewController
    }
    
    static var home: HomeViewController? {
      return UIStoryboard.home.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
    }
    
    static var tabBar: TabBarViewController? {
      return UIStoryboard.tabBar.instantiateViewController(withIdentifier: "TabBarViewController") as? TabBarViewController
    }
    
    static var settings: SettingsViewController? {
      return UIStoryboard.settings.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController
    }
    
    static var users: UsersViewController? {
      return UIStoryboard.users.instantiateViewController(withIdentifier: "UsersViewController") as? UsersViewController
    }
    
    static var profile: ProfileViewController? {
      return UIStoryboard.profile.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
    }
    
    static var chat: ChatViewController? {
      return UIStoryboard.chat.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController
    }
    
    static var map: MapViewController? {
      return UIStoryboard.chat.instantiateViewController(withIdentifier: "MapViewController") as? MapViewController
    }
  }
}
