//
//  UIView+Extension.swift
//
//  Created by Duy Bui on 9/23/18.
//  Copyright © 2018 DuyBui. All rights reserved.
//

import UIKit

extension UIView {
  
  @IBInspectable
  var borderRadius: CGFloat {
    get {
      return layer.cornerRadius
    }
    set {
      layer.cornerRadius = newValue
      clipsToBounds = newValue > 0
    }
  }
  
  @IBInspectable var borderColor: UIColor? {
    set {
      layer.borderColor = newValue?.cgColor
    }
    get {
      guard let color = layer.borderColor else {
        return nil
      }
      return UIColor(cgColor: color)
    }
  }
  @IBInspectable var borderWidth: CGFloat {
    set {
      layer.borderWidth = newValue
    }
    get {
      return layer.borderWidth
    }
  }
  
  func roundBorder(radius: CGFloat = 2, width: CGFloat = 1, color: CGColor = UIColor.clear.cgColor) {
    self.layer.cornerRadius = radius
    self.layer.borderWidth = width
    self.layer.borderColor = color
    self.layer.masksToBounds = true
  }
  
  func makeCircle() {
    self.layer.cornerRadius = self.frame.width / 2
    self.layer.masksToBounds = true
  }
  
  func makeShadow(opacity: Float, radius: CGFloat, height: CGFloat, color: UIColor) {
    self.layer.shadowOpacity = opacity
    self.layer.shadowRadius = radius
    self.layer.shadowColor = color.cgColor
    self.layer.shadowOffset = CGSize(width: 0, height: height)
    self.layer.masksToBounds = false
  }
  
  func addBlurToView(style: UIBlurEffect.Style) {
    let blurEffect = UIBlurEffect(style: style)
    let blurEffectView = UIVisualEffectView(effect: blurEffect)
    blurEffectView.frame = self.bounds
    blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    self.addSubview(blurEffectView)
  }
  
  // OUTPUT 1
  func dropShadow() {
    self.layer.shadowColor = UIColor.black.cgColor
    self.layer.shadowOffset = CGSize(width: 5, height: 5)
    self.layer.shadowOpacity = 0.5
    self.layer.shadowRadius = 25.0
    self.layer.masksToBounds = false
  }
  
  @discardableResult
  func fromNib<T: UIView>() -> T? {
    guard let view = UINib(nibName: String(describing: type(of: self)), bundle: nil).instantiate(withOwner: self, options: nil).first as? T else {
      return nil
    }
    view.frame = bounds
    view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    addSubview(view)
    return view
  }
  
  /// Add border side
  enum ViewSide {
    case Left, Right, Top, Bottom
  }
  
  func addBorder(toSide side: ViewSide, withColor color: UIColor, width: CGFloat) {
    
    let border = CALayer()
    border.backgroundColor = color.cgColor
    
    switch side {
    case .Left:
      border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
    case .Right:
      border.frame = CGRect(x: frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
    case .Top:
      border.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: width)
    case .Bottom:
      border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: frame.size.width, height: width)
    }
    
    self.layer.addSublayer(border)
  }
  
  func makeGradient(startColor: CGColor, endColor: CGColor) {
    let layer = CAGradientLayer()
    layer.startPoint = CGPoint(x: 0.0, y: 0.5)
    layer.endPoint = CGPoint(x: 1.0, y: 0.5)
    layer.frame = self.bounds
    layer.colors = [startColor, endColor]
    
    self.layer.addSublayer(layer)
  }
}
