//
//  NotificationBannerView.swift
//  NotificationBanner
//
//

import UIKit

open class NotificationBannerView: UIView {
  
  var textView: UILabel!
  var imageView: UIImageView!
  var initialBannerHeight: Int = 80
  var currentContentFont: CGFloat = 16
  var currentView: UIWindow!
  var currentHeight: CGFloat = 100

  var springHeight: CGFloat = 0
  var fixHeight: Int = 0
  var fixStatusBarHeight: Int = 0
  
  internal init(view: UIWindow, height: Int) {
    currentView = view
    springHeight = CGFloat(height) * 0.33
    currentHeight = CGFloat(height) + (springHeight)
    initialBannerHeight = height
    super.init(frame: CGRect(x: 0, y: Int(-currentHeight-self.springHeight * 0.33), width: Int(view.bounds.width), height: Int(currentHeight+springHeight / 3) ))
    if !(view.subviews.contains(where: {$0 is (NotificationBannerView)})) {
      self.backgroundColor = backgroundColor
      view.addSubview(self)
    }

  }
  
  required public init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  open func show() {
//    textView.isScrollEnabled = false
    self.isHidden = false
    UIView.animate(
      withDuration: Double(3), delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 3.5, options: [.curveEaseInOut, .allowUserInteraction],
      animations: {
        self.frame.origin.y = -(CGFloat)(self.springHeight)
    },
      completion: { _ in }
    )
  }
  
  open func setParameter(textContent: String = "", textColor: UIColor = .white, backgroundColor: UIColor = .black) {
    if backgroundColor != .black {
      self.backgroundColor = backgroundColor
    }
    if textContent != ""{
      self.textView.text = textContent
    }
    if textColor != .white {
      self.textView.textColor = textColor
    }
  }
  
  internal func dissmiss(completion: @escaping (_ Success: Bool) -> Void) {
    UIView.animate(
      withDuration: Double(0.5), delay: 0, options: [.curveEaseInOut, .allowUserInteraction],
      animations: {
        self.frame.origin.y = -CGFloat(self.currentHeight+self.springHeight * 0.33)
    },
      completion: { _ in
        self.isHidden = true
        self.layer.removeAllAnimations()
        self.removeFromSuperview()
        completion(true)
    }
    )
  }
  
  internal func setBannerContent(textContent: String = "",
                                 textColor: UIColor = .white,
                                 messageContentFont contentFont: CGFloat = 13,
                                 bannerBackgroundColor: UIColor = .blue,
                                 imageName: String? = nil) {
    currentContentFont = contentFont
    
    fixHeight = Int(springHeight)
    fixStatusBarHeight = Int(springHeight * 0.5)
    self.frame = CGRect(x: 0, y: Int(-currentHeight-self.springHeight * 0.33), width: Int(currentView.bounds.width), height: Int(currentHeight+springHeight * 0.33) )

    self.subviews.forEach({ $0.removeFromSuperview() })
    imageView = UIImageView(frame: CGRect(x: 10, y: Int(currentHeight / 2) + 12, width: 25, height: 21))
    imageView.contentMode = .scaleAspectFit
    self.addSubview(imageView)
    
    textView = UILabel(frame: CGRect(x: 44, y: Int(imageView.frame.origin.y), width: Int(currentView.bounds.width - 44), height: Int(currentHeight * 0.35)))
    textView.text = textContent

    textView.textColor = textColor
    textView.font = UIFont.systemFont(ofSize: currentContentFont)
    textView.backgroundColor = UIColor.clear
    textView.numberOfLines = 0
    textView.textAlignment = .left
    textView.sizeToFit()
    self.addSubview(textView)
    if let imageName = imageName {
      imageView.image = UIImage(named: imageName)
    }
    backgroundColor = bannerBackgroundColor
  }
}
