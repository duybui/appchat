//
//  NotificationBanner.swift
//  NotificationBanner
//
//

import UIKit

var bannerView: NotificationBannerView!
var view: UIWindow!

public func showNotificationBanner(messageContent message: String,
                                   messageContentFont contentFont: CGFloat = 13,
                                   bannerHeight height: Int = 80,
                                   bannerHoldTime holdTime: Int = 5,
                                   bannerBackgroundColor: UIColor = .blue, isNetworkBanner: Bool = true,
                                   isAutoDismiss: Bool = false,
                                   messageImageName imageName: String? = nil) {
  
  view = UIApplication.shared.keyWindow
  if view == nil {
    return
  }
  
  if !(view.subviews.contains(where: {$0 is (NotificationBannerView)})) {
    bannerView = NotificationBannerView(view: view, height: height)
    bannerView.isHidden = true
    bannerView.setBannerContent(textContent: message, messageContentFont: contentFont, bannerBackgroundColor: bannerBackgroundColor, imageName: imageName)
    bannerView.imageView.isHidden = !isNetworkBanner
    bannerView.show()
  } else {
    if bannerView.isHidden {
      bannerView.imageView.isHidden = !isNetworkBanner
      bannerView.textView.text = message
      bannerView.textView.sizeToFit()
      bannerView.show()
    }
  }
  // auto dismiss
  if !isNetworkBanner || isAutoDismiss {
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5, execute: {
      dissmissBanner()
    })
  }
}

public func dissmissBanner(completion: @escaping (_ Success: Bool) -> Void = { Success in }) {
  if bannerView == nil || bannerView.isHidden {
    return
  }
  bannerView.dissmiss(completion: completion)
}
